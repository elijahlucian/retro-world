﻿using UnityEngine;

public class AsteroidBehavior : MonoBehaviour
{
    [Range(5000, 30000)]
    public int maxForce;

    private Transform tf;
    private bool changed; 

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
        Rigidbody rb = GetComponent<Rigidbody>();

        Vector3 force = new Vector3(
            Random.Range(-maxForce, maxForce),
            Random.Range(-maxForce, maxForce),
            Random.Range(-maxForce, maxForce)
            );
        rb.AddForce(force);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = tf.position;

        float x = position.x;
        float y = position.y;
        float z = position.z;

        if (x > 5) {
            x = -5;
            changed = true;
        } else if (x < -5) {
            x = 5;
            changed = true;
        }

        if (z > 5) {
            z = -5;
            changed = true;
        } else if (z < -5) {
            z = 5;
            changed = true;
        }

        if(changed) {
            tf.position = new Vector3(x,y,z);
            changed = false;
        }
    }
}
