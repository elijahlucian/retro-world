﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    private AudioSource sfx_hit;
    // Start is called before the first frame update
    void Start()
    {
        sfx_hit = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision) {
        sfx_hit.pitch = Random.Range(.9f, 1.1f);
        sfx_hit.Play();
    }

}
