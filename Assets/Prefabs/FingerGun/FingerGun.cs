﻿using UnityEngine;

public class FingerGun : Interactable
{
    public GameObject bulletPrefab;
    public float bulletForce;
    public AudioSource SFX_shoot;

    private GameObject bullet;

    private void Awake() {
        SFX_shoot.enabled = true;
    }

    public override void Action() {
        bullet = Instantiate(bulletPrefab);
        bullet.transform.position = transform.position + transform.forward * .35f + transform.up * .05f + transform.right * .005f;
        bullet.transform.rotation = transform.rotation;
        bullet.GetComponent<Rigidbody>().AddRelativeForce(bullet.transform.forward * bulletForce);  
        bullet.SetActive(true);

        SFX_shoot.Play();
    }
}
