﻿using System.Collections.Generic;
using Valve.VR;
using UnityEngine;

public class GrabShit : MonoBehaviour
{
    public SteamVR_Action_Boolean gripAction;
    public SteamVR_Action_Boolean triggerAction;

    private SteamVR_Behaviour_Pose pose;
    private FixedJoint joint;
    private Interactable item;
    private List<Interactable> interactions = new List<Interactable>();

    void Awake()
    {
        pose = GetComponent<SteamVR_Behaviour_Pose>();
        joint = GetComponent<FixedJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        if(gripAction.GetStateDown(pose.inputSource)) {
            Pickup();
        }

        if(triggerAction.GetLastStateDown(pose.inputSource)) {
            if (!item) return;

            item.Action();
        }

        if(gripAction.GetStateUp(pose.inputSource)) {
            Drop(); 
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (!isInteractable(other)) {
            return;
        }

        interactions.Add(other.gameObject.GetComponent<Interactable>());
    }

    private void OnTriggerExit(Collider other) {
        if (!isInteractable(other)) {
            return;
        }

        interactions.Remove(other.gameObject.GetComponent<Interactable>());
    }

    public void Pickup() {
        item = GetNearest();
        if (!item) return;

        if (item.activeHand) item.activeHand.Drop();

        item.ApplyOffset(transform);

        // TODO: already held check
        item.transform.position = transform.position;
        Rigidbody target = item.GetComponent<Rigidbody>();
        joint.connectedBody = target;
        item.activeHand = this;
    }

    public void Drop() {
        if (!item) return;

        // apply velocity
        Rigidbody target = item.GetComponent<Rigidbody>();
        target.velocity = pose.GetVelocity();
        target.angularVelocity = pose.GetAngularVelocity();

        joint.connectedBody = null;

        // clear
        item.activeHand = null;
        item = null;
    }

    private bool isInteractable(Collider other) {
        return other.gameObject.CompareTag("Interactable");
    }

    private Interactable GetNearest() {
        Interactable nearest = null;

        float minDistance = float.MaxValue;
        float distance = 0;

        foreach (Interactable item in interactions) {
            distance = (item.transform.position - transform.position).sqrMagnitude;
            if (distance < minDistance) {
                minDistance = distance;
                nearest = item;
            }
        }

        return nearest;
    }

}

