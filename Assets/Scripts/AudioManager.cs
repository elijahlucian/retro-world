﻿using UnityEngine;
using System;
using UnityEngine.Audio;


public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;

    void Awake() {
        if (!instance) {
            instance = this;
        } else {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound sound in sounds) {
            AudioSource source = gameObject.AddComponent<AudioSource>();
            source.clip = sound.clip;
            source.volume = sound.volume;
            source.pitch = sound.pitch;
            sound.source = source;
        }
    }
    
    public void Play(string name) {
        Sound sound = Array.Find(sounds, s => s.name == name);
        if (!sound.source) return;

        sound.source.Play();
    }
}
