﻿using Valve.VR;
using UnityEngine;

public class TeleportPlayer : MonoBehaviour
{
    public SteamVR_Input_Sources handType;
    public SteamVR_Behaviour_Pose controllerPose;
    public SteamVR_Action_Boolean teleportAction;
    public GameObject laserPrefab;
    public Transform cameraRigTransform;
    public GameObject reticlePrefab;
    public Transform headTransform;
    public Vector3 reticleOffset;
    public LayerMask teleportMask;

    private GameObject reticle;
    private Transform reticleTransform;
    private GameObject laser;
    private Transform laserTransform;
    private Vector3 hitPoint;
    private bool shouldTeleport;

    // Start is called before the first frame update
    void Start()
    {
        laser = Instantiate(laserPrefab);
        laserTransform = laser.transform;

        reticle = Instantiate(reticlePrefab);
        reticleTransform = reticle.transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (teleportAction.GetState(handType)) {
            // TODO: Change Teleport Action to just touch, Then move on click
            RaycastHit hit;

            if(Physics.Raycast(controllerPose.transform.position, transform.forward, out hit, 100, teleportMask)) {

                hitPoint = hit.point;
                ShowLaser(hit);

                reticle.SetActive(true);
                reticleTransform.position = hitPoint + reticleOffset;

                
                shouldTeleport = true;
                
            }
        }

        else {
            reticle.SetActive(false);
            laser.SetActive(false);
        }

        if (teleportAction.GetLastStateUp(handType) && shouldTeleport) {
            Teleport();
        }

    }

    private void ShowLaser(RaycastHit hit) {
        laser.SetActive(true);
        laserTransform.position = Vector3.Lerp(controllerPose.transform.position, hitPoint, .5f);
        laserTransform.LookAt(hitPoint);
        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y, hit.distance);
    }

    private void Teleport() {
        shouldTeleport = false;
        reticle.SetActive(false);
        Vector3 difference = cameraRigTransform.position - headTransform.position;
        difference.y = 0;
        cameraRigTransform.position = hitPoint + difference;
    }
}
