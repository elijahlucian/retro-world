﻿using UnityEngine;
using Valve.VR;

[RequireComponent(typeof(Rigidbody))]
public class Interactable : MonoBehaviour
{
    public GrabShit activeHand;
    public Vector3 offset;


    public virtual void Action() {
    }

    public void ApplyOffset(Transform hand) {
        transform.SetParent(hand);
        transform.localRotation = Quaternion.identity; // TODO. change rotation.
        transform.localPosition = offset;
        transform.SetParent(null);
    }

}
